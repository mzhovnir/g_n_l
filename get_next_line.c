/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mzhovnir <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/02 17:59:59 by mzhovnir          #+#    #+#             */
/*   Updated: 2018/05/02 18:00:04 by mzhovnir         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

# include <stdio.h>
# include <stdlib.h>
# include <unistd.h>
# include <fcntl.h>
# include <sys/stat.h>
# include <sys/types.h>

# define BUFFER_SIZE    8

char    *ft_strchr(const char *s, int c)
{
    char    *str;
    char    ch;

    str = (char *)s;
    ch = (char)c;
    while (*str)
    {
        if (*str == ch)
            return (str);
        str++;
    }
    if ((*str == '\0') && (ch == '\0'))
        return (str);
    return (NULL);
}

char    *ft_strnew(size_t size)
{
    char    *str;
    size_t  i;

    i = 0;
    str = (char *)malloc(sizeof(char) * (size + 1));
    if (str == NULL)
        return (NULL);
    while (i <= size)
    {
        str[i] = '\0';
        i++;
    }
    return (str);
}

size_t  ft_strlen(const char *str)
{
    int i;

    i = 0;
    while (str[i])
        i++;
    return (i);
}

char    *ft_strjoin(char const *s1, char const *s2)
{
    char    *str_new;
    size_t  i;

    i = 0;
    if (s1 == NULL)
        return ((char *)s2);
    if (s2 == NULL)
        return ((char *)s1);
    str_new = (char *)malloc(sizeof(char) *
(ft_strlen(s1) + ft_strlen(s2) + 1));
    if (str_new == NULL)
        return (NULL);
    while (*s1)
    {
        str_new[i++] = *s1;
        s1++;
    }
    while (*s2)
    {
        str_new[i++] = *s2;
        s2++;
    }
    str_new[i] = '\0';
    return (str_new);
}

char    *ft_strsub(char const *s, unsigned int start, size_t len)
{
    char    *new_str;
    size_t  i;

    i = 0;
    if (s == NULL)
        return (NULL);
    new_str = (char *)malloc(sizeof(char) * (len + 1));
    if (new_str == NULL)
        return (NULL);
    while (len)
    {
        new_str[i] = s[start];
        i++;
        start++;
        len--;
    }
    new_str[i] = '\0';
    return (new_str);
}

char    *ft_strdup(const char *str)
{
    int     i;
    int     len;
    char    *str_new;

    len = 0;
    while (str[len])
        len++;
    str_new = (char*)malloc(sizeof(*str_new) * (len + 1));
    if (str_new == NULL)
        return (NULL);
    i = 0;
    while (i < len)
    {
        str_new[i] = str[i];
        i++;
    }
    str_new[i] = '\0';
    return (str_new);
}

/**********************************************************************************************/

int     get_next_line(const int fd, char **line)
{
    static char     *str[4864];
    char            buffer[BUFFER_SIZE + 1];
    char            *tmp;
    int             ret;
    int             length;

    if (fd < 0 || line == NULL)
        return (-1);
    if ((ret = read(fd, buffer, BUFFER_SIZE)) < 0)
        return (-1);
    printf("In buffer now:\n-->%s<--\n", buffer);

    if (ret > 0)
    {
        buffer[ret] = '\0';
        if (str[fd] == NULL)
            str[fd] = ft_strnew(0);
        tmp = ft_strjoin(str[fd], buffer);
        str[fd] = tmp;
        printf("str[fd] -->%s<--\n", str[fd]);
    }

    return (1);
}

int        main(int argc, char **argv)
{
    int     fd;
    char    *line;

    if (argc != 2)
    {
        write(1, "Too few arguments\n", 18);
        exit(1);
    }
    if ((fd = open(argv[1], O_RDONLY)) < 0)
        write(1, "error1\n", 6);
	while (get_next_line(fd, &line) == 1)
	{
		get_next_line(fd, &line);
		printf("line -->%s", line);
		free(&line);
	}
    if (close(fd) != 0)
    {
        write(1, "Didnt close!\n", 6);
        exit(1);
    }
    return (0);
}
