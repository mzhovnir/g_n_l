#include <stdio.h>

/*void foo()
{
    int a = 10;
    static int sa = 10;

    a += 5;
    sa += 5;

    printf("a = %d, sa = %d\n", a, sa);
}


int main()
{
    int i;

	i = 0;
    while (i < 10)
    {
    	foo();
    	i++;
    }
}*/

int main()
{
	for(int ix=0; ix < 10; ix++)
	{
	  for(int iy = 0; iy < 10; iy++)
	  {
	    static int number_of_times = 0;
	    number_of_times++;
	    printf("ix = %d, iy = %d, number_of_times = %d\n", ix, iy, number_of_times);
	  }
	}
}