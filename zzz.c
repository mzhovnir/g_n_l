# include <stdio.h>
# include <stdlib.h>
# include <unistd.h>
# include <fcntl.h>
# include <sys/stat.h>
# include <sys/types.h>

# define BUFFER_SIZE    8

char    *ft_strchr(const char *s, int c)
{
    char    *str;
    char    ch;

    str = (char *)s;
    ch = (char)c;
    while (*str)
    {
        if (*str == ch)
            return (str);
        str++;
    }
    if ((*str == '\0') && (ch == '\0'))
        return (str);
    return (NULL);
}

char    *ft_strnew(size_t size)
{
    char    *str;
    size_t  i;

    i = 0;
    str = (char *)malloc(sizeof(char) * (size + 1));
    if (str == NULL)
        return (NULL);
    while (i <= size)
    {
        str[i] = '\0';
        i++;
    }
    return (str);
}

size_t  ft_strlen(const char *str)
{
    int i;

    i = 0;
    while (str[i])
        i++;
    return (i);
}

char    *ft_strjoin(char const *s1, char const *s2)
{
    char    *str_new;
    size_t  i;

    i = 0;
    if (s1 == NULL)
        return ((char *)s2);
    if (s2 == NULL)
        return ((char *)s1);
    str_new = (char *)malloc(sizeof(char) *
(ft_strlen(s1) + ft_strlen(s2) + 1));
    if (str_new == NULL)
        return (NULL);
    while (*s1)
    {
        str_new[i++] = *s1;
        s1++;
    }
    while (*s2)
    {
        str_new[i++] = *s2;
        s2++;
    }
    str_new[i] = '\0';
    return (str_new);
}

char    *ft_strsub(char const *s, unsigned int start, size_t len)
{
    char    *new_str;
    size_t  i;

    i = 0;
    if (s == NULL)
        return (NULL);
    new_str = (char *)malloc(sizeof(char) * (len + 1));
    if (new_str == NULL)
        return (NULL);
    while (len)
    {
        new_str[i] = s[start];
        i++;
        start++;
        len--;
    }
    new_str[i] = '\0';
    return (new_str);
}

char    *ft_strdup(const char *str)
{
    int     i;
    int     len;
    char    *str_new;

    len = 0;
    while (str[len])
        len++;
    str_new = (char*)malloc(sizeof(*str_new) * (len + 1));
    if (str_new == NULL)
        return (NULL);
    i = 0;
    while (i < len)
    {
        str_new[i] = str[i];
        i++;
    }
    str_new[i] = '\0';
    return (str_new);
}

/**********************************************************************************************/

int     get_next_line(const int fd, char **line)
{
    static char     *str[4864];//Сюда запишем остаток от считаного фрагмента (после \n)
    char            buffer[BUFFER_SIZE + 1];//буффер
    char            *tmp;//Сюда запишем результат джойна буффера и остатка
    int             ret;//количество возвращаемых байт
    int             length;//Длина считаного фрагмента до символа '\n'

    if (fd < 0 || line == NULL)//Если строка не считалась или строка пустая --> возвращаем (-1)
        return (-1);
    ret = read(fd, buffer, BUFFER_SIZE);
    printf("----------------------------\nret = %d\n", ret);
    printf("In buffer now:\n-->%s<--\n", buffer);
    
    if (ret > 0)
    {
        buffer[ret] = '\0';//В последний элемент буффера записываем '\0'
        if (str[fd] == NULL)
            str[fd] = ft_strnew(1);//Выделяем память под песочницу, пока оставим ее пустой
        tmp = ft_strjoin(str[fd], buffer);//Во временный массив запишем результат 
        str[fd] = tmp;
        printf("str[fd] -->%s<--\n", str[fd]);
        /*if (ft_strchr(buffer, '\n'))
            printf("Сдесь есть долбанный слэш эн!\n");
        else
            printf("Нихрена не угадала!\n");*/
        length = 0;
        while (str[fd][length] != '\n' && str[fd][length] != '\0')//Просчитываем длину фрагмента из буффера от начала и до '\n'
            length++;
        if (str[fd][length] == '\n')//Если в буффере мы нашли символ '\n', тогда 
        {
            printf("N находится на позиции --> = %d\n", length);//Проверяем на каком индексе находится символ '\0'
            *line = ft_strsub(str[fd], 0, length);//В line записываем из буффера все, что до \n
            printf("line -->%s<--\n", *line);//Смотрим, что отображается в line
            tmp = ft_strsub(str[fd], length + 1, ft_strlen(str[fd]) - length);//В tmp записываем остаток из буффера после \n
            printf("tmp -->%s<--\n", tmp);
            printf("Previous version of str[fd] -->%s<--\n", str[fd]);//До очистки
            free(str[fd]);
            str[fd] = tmp;
            printf("Modified version of str[fd] -->%s<--\n", str[fd]);//После очистки
        }
        else if (str[fd][length] == '\0')
        {
            *line = ft_strdup(str[fd]);
            ft_strdel(&str[fd])
        }
    }
    if (ret < 0)//Проверяем считался ли фрагмент
        return (-1);
    if (ret == 0)
        return (0);
    return (1);
}

int        main(int argc, char **argv)
{
    int     fd;
    char    *line;

    if (argc != 2)
    {
        write(1, "Too few arguments\n", 18);
        exit(1);
    }
    if ((fd = open(argv[1], O_RDONLY)) < 0)
        write(1, "error1\n", 6);
    get_next_line(fd, &line);
    printf("Final line 1-->%s\n", line);
    get_next_line(fd, &line);
    printf("Final line 2-->%s\n", line);
    get_next_line(fd, &line);
    printf("Final line 3-->%s\n", line);
    get_next_line(fd, &line);
    printf("Final line 4-->%s\n", line);
    get_next_line(fd, &line);
    printf("Final line 5-->%s\n", line);
    get_next_line(fd, &line);
    printf("End line 6-->%s\n", line);
    if (close(fd) != 0)
    {
        write(1, "Didnt close!\n", 6);
        exit(1);
    }
    return (0);
}