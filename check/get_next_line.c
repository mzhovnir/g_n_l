/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mzhovnir <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/23 18:22:39 by mzhovnir          #+#    #+#             */
/*   Updated: 2018/05/23 20:00:34 by mzhovnir         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"

int		read_line(char **str, int fd, char *tmp, int *ret)
{
	char			buffer[BUFF_SIZE + 1];

	while ((*ret = read(fd, buffer, BUFF_SIZE)) > 0)
	{
		buffer[*ret] = '\0';
		if (str[fd] == NULL)
			str[fd] = ft_strnew(1);
		tmp = ft_strjoin(str[fd], buffer);
		free(str[fd]);
		str[fd] = tmp;
		if (ft_strchr(buffer, '\n'))
			break ;
	}
	return (1);
}

void	line_creator(char **str, int index, char **line, int fd)
{
	char			*tmp;

	while (str[fd][index] != '\n' && str[fd][index] != '\0')
		index++;
	if (str[fd][index] == '\n')
	{
		*line = ft_strsub(str[fd], 0, index);
		tmp = ft_strsub(str[fd], index + 1, ft_strlen(str[fd]) - index);
		free(str[fd]);
		str[fd] = tmp;
	}
	else if (str[fd][index] == '\0')
	{
		*line = ft_strdup(str[fd]);
		ft_strdel(&str[fd]);
	}
}

int		get_next_line(const int fd, char **line)
{
	static char		*str[4864];
	char			*tmp;
	int				ret;
	int				index;

	index = 0;
	tmp = NULL;
	if (fd < 0 || line == NULL || read(fd, 0, 0) < 0 || fd > 4864)
		return (-1);
	read_line(str, fd, tmp, &ret);
	if (ret == -1)
		return (-1);
	if (ret == 0 && (str[fd] == NULL || str[fd][0] == '\0'))
		return (0);
	line_creator(str, index, line, fd);
	return (1);
}

int		main(int argc, char **argv)
{
	int				fd;
	int				fd1;
	char			*line;

	argc = 0;
	fd1 = open(argv[2], O_RDONLY);
	fd = open(argv[1], O_RDONLY);
	while (get_next_line(fd, &line) == 1)
	{
		printf("%s\n", line);
		free(line);
	}
	printf("\n\n");
	while (get_next_line(fd1, &line) == 1)
	{
		printf("%s\n", line);
		free(line);
	}
	system("leaks get_next_line11");
	return (0);
}